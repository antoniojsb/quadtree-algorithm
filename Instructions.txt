Parallel Quadtree Implementation by Ant�nio Barreto & Sara Lu�s.

Instructions:

	- Opencv Plugin
	Download from https://opencv.org/releases.html

	- TBB
	Download from https://www.threadingbuildingblocks.org/download

	- (Optional) Concurrency Visualizer for Visual Studio
	Download from https://docs.microsoft.com/pt-pt/visualstudio/profiling/concurrency-visualizer
	
	- Set image path
	Main.cpp 112. String imagePath = "ImagePath/name.png"; (e.g "C:/Users/anton/Desktop/rally.png" )
	
	- Set algorithm Threshold
	Main.cpp 114. double threshold = value;  (the bigger the value the more compressed the image will be.)
	