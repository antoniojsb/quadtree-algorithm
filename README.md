# Quadtree Algorithm for image compression #
Quadtree decomposition is a technique used to obtain a representation of an image at different resolution levels.

The algorithm is a tree data structure in which a split and merge operation takes place and each internal node has exactly four children ( result of recursively subdividing it into four quadrants or regions).

![Alt Text](https://dahtah.github.io/imager/quadtrees_parrots.gif)

### What is this repository? ###

Project developed by Ant�nio Barreto & Sara Lu�s for 2017/2018 Parallel Systems course at Ensimag.

![Alt Text](http://ensimag.grenoble-inp.fr/images/ensimag/logo.gif)

### How to get set up? ###

	- Opencv Plugin
	Download from https://opencv.org/releases.html

	- TBB
	Download from https://www.threadingbuildingblocks.org/download

	- (Optional) Concurrency Visualizer for Visual Studio
	Download from https://docs.microsoft.com/pt-pt/visualstudio/profiling/concurrency-visualizer
	
	- Set image path (!!OpenCV only accepts .png files!!)
	Main.cpp 112. String imagePath = "ImagePath/name.png"; (e.g "C:/Users/anton/Desktop/rally.png" )
	
	- Set algorithm Threshold
	Main.cpp 114. double threshold = value;  (the bigger the value the more compressed the image will be.)
	
### Reference used: ###
[Quadtrees Implementation](http://devmag.org.za/2011/02/23/quadtrees-implementation/ "Quadtrees Implementation referenced")
	