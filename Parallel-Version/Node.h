#pragma once
class Node
{

private:
	int value = 0;
	void* children[4];
	int x = 0, y = 0;
	int width = 0, height = 0;
	int defaultValue = 0;
	bool hasChildren = false;
	bool hasDefault = false;

public:
	Node(int data[], int matrixPad, int x, int y, int width, int height, double threshold);

	virtual int get(int i, int j);

	virtual int getChildCount(bool countDataNodesOnly);

	virtual int averageMerge(int data[], int matrixPad, int x, int y, int width, int height);

	virtual double measureDetail(int data[], int matrixPad, int x, int y, int width, int height);

	virtual int getWidth();

	virtual int getHeight();

};
