#pragma once

#include "Node.h"


class QuadTree {

public:
	Node * root;
	QuadTree(int data[], int matWidth, int matHeight, double threshold);
	virtual int get(int i, int j);

};
