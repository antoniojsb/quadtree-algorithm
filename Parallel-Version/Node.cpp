
#include "Node.h"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_reduce.h"
#include "tbb/task_group.h"

using namespace tbb;
using namespace std;

Node::Node(int data[], int matrixPad, int x, int y, int width, int height, double threshold)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->defaultValue = 0;
	this->hasDefault = true;

	if ((width == 1) || (height == 1) || (measureDetail(data, matrixPad, x, y, width, height) <= threshold))
	{
		value = averageMerge(data, matrixPad, x, y, width, height);
	}
	else
	{
		int task_threshold = 100; //500 - 0.0167017s  //200 - 0.015151s //100 - 0.0136324s //20 - 0.0193622s -- Threshold test( Manual Tunning).

		int area1, area2, area3, area4;
		this->hasChildren = true;
		task_group g;
		area1 = (width / 2)*(height / 2);
		area2 = (width - width / 2)*(height / 2);
		area3 = (width / 2)*(height - height / 2);
		area4 = (width - width / 2)*(height - height / 2);

		//task creation for nodes with areas bigger than the threshold for granularity control purposes.
		if (area1>task_threshold)
			g.run([&] {children[0] = new Node(data, matrixPad, x, y, width / 2, height / 2, threshold); });
		else
			children[0] = new Node(data, matrixPad, x, y, width / 2, height / 2, threshold);
		if (area2>task_threshold)
			g.run([&] {children[1] = new Node(data, matrixPad, x + width / 2, y, width - width / 2, height / 2, threshold); });
		else
			children[1] = new Node(data, matrixPad, x + width / 2, y, width - width / 2, height / 2, threshold);
		if (area3>task_threshold)
			g.run([&] {children[2] = new Node(data, matrixPad, x, y + height / 2, width / 2, height - height / 2, threshold); });
		else
			children[2] = new Node(data, matrixPad, x, y + height / 2, width / 2, height - height / 2, threshold);
		if (area4>task_threshold)
			g.run([&] {children[3] = new Node(data, matrixPad, x + width / 2, y + height / 2, width - width / 2, height - height / 2, threshold); });
		else
			children[3] = new Node(data, matrixPad, x + width / 2, y + height / 2, width - width / 2, height - height / 2, threshold);
		g.wait();


	}
}




int Node::get(int i, int j)
{
	int thatvalue;
	if (hasChildren == true) //this is not a leaf
	{
		if (i < x + width / 2)
		{
			if (j < y + height / 2)
			{
				thatvalue = (static_cast<Node*>(children[0]))->get(i, j);
				return thatvalue;
			}
			else
			{
				thatvalue = (static_cast<Node*>(children[2]))->get(i, j);
				return thatvalue;
			}
		}
		else
		{
			if (j < y + height / 2)
			{
				thatvalue = (static_cast<Node*>(children[1]))->get(i, j);
				return thatvalue;
			}
			else
			{
				thatvalue = (static_cast<Node*>(children[3]))->get(i, j);
				return thatvalue;
			}
		}
	}
	else
	{
		if (((i == x) || (j == y)) && (hasChildren == false))
		{
			thatvalue = 0;
			return thatvalue; //change to " return value; " to hide the black splitting lines from the result image.
		}
		else
		{
			thatvalue = value;
			return thatvalue;
		}
	}
}

int Node::getChildCount(bool countDataNodesOnly)
{
	int count = countDataNodesOnly ? 0 : 1; //self

	if (hasChildren == true)
	{
		count += (static_cast<Node*>(children[0]))->getChildCount(countDataNodesOnly);
		count += (static_cast<Node*>(children[1]))->getChildCount(countDataNodesOnly);
		count += (static_cast<Node*>(children[2]))->getChildCount(countDataNodesOnly);
		count += (static_cast<Node*>(children[3]))->getChildCount(countDataNodesOnly);
	}

	return count;
}

class Merge {
	int* my_data;
	int my_matrixPad;
public:
	int my_redSum;
	int my_greenSum;
	int my_blueSum;
	int rgb;
	void operator()(const blocked_range2d<int>& t) {
		int* data = my_data;
		int redSum = my_redSum;
		int greenSum = my_greenSum;
		int blueSum = my_blueSum;
		for (int i = t.rows().begin(); i < t.rows().end(); i++) {
			for (int j = t.cols().begin(); j < t.cols().end(); j++) {
				rgb = data[j*my_matrixPad + i];
				redSum += (rgb >> 16) & 0x000000FF;
				greenSum += (rgb >> 8) & 0x000000FF;
				blueSum += (rgb & 0x000000FF);
			}
		}
		my_redSum = redSum;
		my_greenSum = greenSum;
		my_blueSum = blueSum;
	}

	Merge(Merge& x, split) :
		my_data(x.my_data), my_matrixPad(x.my_matrixPad), my_redSum(0), my_greenSum(0), my_blueSum(0)
	{}

	void join(const Merge& y) { my_redSum += y.my_redSum; my_greenSum += y.my_greenSum; my_blueSum += y.my_blueSum; }

	Merge(int data[], int matrixPad) :
		my_data(data), my_matrixPad(matrixPad), my_redSum(0), my_greenSum(0), my_blueSum(0)
	{}
};

int Node::averageMerge(int data[], int matrixPad, int x, int y, int width, int height) {
	Merge calc(data, matrixPad);
	int pixelCount = width * height;
	parallel_reduce(blocked_range2d<int>(y, y + height, 1000, x, x + width, 1000),
		calc);
	return ((calc.my_redSum / pixelCount & 0x0ff) << 16) | ((calc.my_greenSum / pixelCount & 0x0ff) << 8) | (calc.my_blueSum / pixelCount & 0x0ff);

}


class Detail {
	int* my_data;
	int my_matrixPad;
	double my_redAvg, my_greenAvg, my_blueAvg;
public:
	double my_redSum;
	double my_greenSum;
	double my_blueSum;
	int rgb;
	double red, green, blue;
	void operator()(const blocked_range2d<int>& k) {
		int* data = my_data;
		double redSum = my_redSum;
		double greenSum = my_greenSum;
		double blueSum = my_blueSum;
		for (int i = k.rows().begin(); i < k.rows().end(); i++) {
			for (int j = k.cols().begin(); j < k.cols().end(); j++) {
				rgb = data[j*my_matrixPad + i];
				red = (rgb >> 16) & 0x000000FF;
				green = (rgb >> 8 & 0x000000FF);
				blue = (rgb & 0x000000FF);

				redSum += (red - my_redAvg)*(red - my_redAvg);
				greenSum += (green - my_greenAvg)*(green - my_greenAvg);
				blueSum += (blue - my_blueAvg)*(blue - my_blueAvg);
			}
		}

		my_redSum = redSum;
		my_greenSum = greenSum;
		my_blueSum = blueSum;
	}

	Detail(Detail& x, split) :
		my_data(x.my_data), my_matrixPad(x.my_matrixPad), my_redSum(0), my_greenSum(0), my_blueSum(0)
	{}

	void join(const Detail& y) { my_redSum += y.my_redSum; my_greenSum += y.my_greenSum; my_blueSum += y.my_blueSum; }

	Detail(int data[], int matrixPad, double redAvg, double greenAvg, double blueAvg) :
		my_data(data), my_matrixPad(matrixPad), my_redAvg(redAvg), my_greenAvg(greenAvg), my_blueAvg(blueAvg), my_redSum(0), my_greenSum(0), my_blueSum(0)
	{}
};

double Node::measureDetail(int data[], int matrixPad, int x, int y, int width, int height)
{
	double redSum = 0;
	double greenSum = 0;
	double blueSum = 0;
	double pixelCount = width * height;
	int rgb, red, green, blue;
	Merge calc(data, matrixPad);
	parallel_reduce(blocked_range2d<int>(y, y + height, 1000, x, x + width, 1000),
		calc);


	double redAvg = calc.my_redSum / pixelCount;
	double greenAvg = calc.my_greenSum / pixelCount;
	double blueAvg = calc.my_blueSum / pixelCount;


	Detail calc2(data, matrixPad, redAvg, greenAvg, blueAvg);
	parallel_reduce(blocked_range2d<int>(y, y + height, 1000, x, x + width, 1000),
		calc2);



	return calc2.my_redSum / (pixelCount * 255 * 255) + calc2.my_greenSum / (pixelCount * 255 * 255) + calc2.my_blueSum / (pixelCount * 255 * 255);
}

int Node::getWidth()
{
	return width;
}

int Node::getHeight()
{
	return height;
}

