
#include "Node.h"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_reduce.h"
#include "tbb/task_group.h"

using namespace tbb;
using namespace std;

Node::Node(int data[], int matrixPad, int x, int y, int width, int height, double threshold)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->defaultValue = 0;
	this->hasDefault = true;

	if ((width == 1) || (height == 1) || (measureDetail(data, matrixPad, x, y, width, height) <= threshold))
	{
		value = averageMerge(data, matrixPad, x, y, width, height);
	}
	else
	{
		this->hasChildren = true;

		children[0] = new Node(data, matrixPad, x, y, width / 2, height / 2, threshold);
		children[1] = new Node(data, matrixPad, x + width / 2, y, width - width / 2, height / 2, threshold);
		children[2] = new Node(data, matrixPad, x, y + height / 2, width / 2, height - height / 2, threshold);
		children[3] = new Node(data, matrixPad, x + width / 2, y + height / 2, width - width / 2, height - height / 2, threshold);
	}
}




int Node::get(int i, int j)
{
	int thatvalue;
	if (hasChildren == true) //this is not a leaf
	{
		if (i < x + width / 2)
		{
			if (j < y + height / 2)
			{
				thatvalue = (static_cast<Node*>(children[0]))->get(i, j);
				return thatvalue;
			}
			else
			{
				thatvalue = (static_cast<Node*>(children[2]))->get(i, j);
				return thatvalue;
			}
		}
		else
		{
			if (j < y + height / 2)
			{
				thatvalue = (static_cast<Node*>(children[1]))->get(i, j);
				return thatvalue;
			}
			else
			{
				thatvalue = (static_cast<Node*>(children[3]))->get(i, j);
				return thatvalue;
			}
		}
	}
	else
	{
		if (((i == x) || (j == y)) && (hasChildren == false))
		{
			thatvalue = 0;
			return thatvalue; //change to " return value; " to hide the black splitting lines from the result image.
		}
		else
		{
			thatvalue = value;
			return thatvalue;
		}
	}
}

int Node::getChildCount(bool countDataNodesOnly)
{
	int count = countDataNodesOnly ? 0 : 1; //self

	if (hasChildren == true)
	{
		count += (static_cast<Node*>(children[0]))->getChildCount(countDataNodesOnly);
		count += (static_cast<Node*>(children[1]))->getChildCount(countDataNodesOnly);
		count += (static_cast<Node*>(children[2]))->getChildCount(countDataNodesOnly);
		count += (static_cast<Node*>(children[3]))->getChildCount(countDataNodesOnly);
	}

	return count;
}

int Node::averageMerge(int data[], int matrixPad, int x, int y, int width, int height)
{
	int redSum = 0;
	int greenSum = 0;
	int blueSum = 0;
	int pixelCount = width * height;
	int rgb;

	for (int i = y; i < y + height; i++)
	{
		for (int j = x; j < x + width; j++)
		{
			rgb = data[j*matrixPad + i];
			redSum += (rgb >> 16) & 0x000000FF;
			
			greenSum += (rgb >> 8) & 0x000000FF;
			
			blueSum += (rgb & 0x000000FF);
			
		}
	}
	return ((redSum / pixelCount & 0x0ff) << 16) | ((greenSum / pixelCount & 0x0ff) << 8) | (blueSum / pixelCount & 0x0ff);
}

double Node::measureDetail(int data[], int matrixPad, int x, int y, int width, int height)
{
	double redSum = 0;
	double greenSum = 0;
	double blueSum = 0;
	double pixelCount = width * height;
	int rgb, red, green, blue;

	for (int i = y; i < y + height; i++)
	{
		for (int j = x; j < x + width; j++)
		{
			rgb = data[j*matrixPad + i];
			redSum += (rgb >> 16) & 0x000000FF;
			greenSum += (rgb >> 8 & 0x000000FF);
			blueSum += (rgb & 0x000000FF);
		}
	}


	double redAvg = redSum / pixelCount;
	double greenAvg = greenSum / pixelCount;
	double blueAvg = blueSum / pixelCount;


	redSum = 0;
	greenSum = 0;
	blueSum = 0;

	for (int i = y; i < y + height; i++)
	{
		for (int j = x; j < x + width; j++)
		{
			rgb = data[j*matrixPad + i];
			red = (rgb >> 16) & 0x000000FF;
			green = (rgb >> 8 & 0x000000FF);
			blue = (rgb & 0x000000FF);
			redSum += (red - redAvg)*(red - redAvg);
			greenSum += (green - greenAvg)*(green - greenAvg);
			blueSum += (blue - blueAvg)*(blue - blueAvg);
		}
	}
	return redSum / (pixelCount * 255 * 255) + greenSum / (pixelCount * 255 * 255) + blueSum / (pixelCount * 255 * 255);
}

int Node::getWidth()
{
	return width;
}

int Node::getHeight()
{
	return height;
}

