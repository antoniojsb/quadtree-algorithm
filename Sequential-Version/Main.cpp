#include "QuadTree.h"
#include <string>
#include <vector>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <cvmarkersobj.h>  

#include <tbb/tick_count.h>
#include "tbb/task_scheduler_init.h"



using namespace Concurrency::diagnostic;
using namespace cv;
using namespace std;

static tbb::tick_count startTick, endTick;


int* img2matrix(Mat image, int width, int height) {

	int *colors = new int[width*height];
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			Vec3b c = image.at<Vec3b>(Point(j, i));
			int b = c[0];
			int g = c[1];
			int r = c[2];
			colors[j*height + i] = ((r & 0x0ff) << 16) | ((g & 0x0ff) << 8) | (b & 0x0ff);
		}
	}
	return colors;
}


Mat quadtreeCompress(Mat image, double threshold2){

	int width = image.size().width;
	int height = image.size().height;
	double threshold = threshold2 / 300.0;
	Vec3b color;
	int rgbCoded;

	// Start of the Concurrency Visualizer Plugin Marker for the image to matrix conversion.
	marker_series inputMSeries(_T("Image Reading"));
	span *inputMSeriessSpan = new span(inputMSeries, 3, _T("Image To Matrix:"));
	inputMSeries.write_flag(1, _T("flag"));

	// create int matrix of rgb values.
	int* colors = img2matrix(image, width, height);

	// End of the Concurrency Visualizer Plugin Marker for the image to matrix conversion.
	delete inputMSeriessSpan;

	// retrieve the number of threads available in the system using tbb.
	int n = tbb::task_scheduler_init::default_num_threads();
	
		// Construct task scheduler with n threads.
		tbb::task_scheduler_init init(n);

		// Start timer from tbb.
		startTick = tbb::tick_count::now();

		// Start of the Concurrency Visualizer Plugin Marker for the Quadtree operation.
		marker_series flagSeries(_T("Parallel Code:"));
		span *flagSeriesSpan = new span(flagSeries, 2, _T("Quadtree"));
		flagSeries.write_flag(1, _T("flag"));
		
		//Quadtree algorithm fucntion call.
		QuadTree *quadTree = new QuadTree(colors, width, height, threshold);

		// End of the Concurrency Visualizer Plugin marker for the Quadtree operation.
		delete flagSeriesSpan;

		// End timer from tbb.
		endTick = tbb::tick_count::now();

		// Quadtree algorithm compilation time print.
		printf("Time Taken: %g seconds with %i threads.\n", (endTick - startTick).seconds(), n);
	
		// Start of the Concurrency Visualizer Plugin Marker for the matrix to image conversion.
		marker_series outputSeries(_T("Image Reading"));
		span *outputSeriesSpan = new span(outputSeries, 1, _T("Image Output"));
		outputSeries.write_flag(1, _T("flag"));

	// Create empty result image
	Mat outImage(height, width, CV_8UC3, Scalar(0, 0, 0));

	// Fill result image
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			rgbCoded = quadTree->get(j, i);
			outImage.at<Vec3b>(Point(j, i)) = { (unsigned char)((rgbCoded) & 0x000000FF),(unsigned char)((rgbCoded >> 8) & 0x000000FF),(unsigned char)((rgbCoded >> 16) & 0x000000FF) };
		}
	}

	// End of the Concurrency Visualizer Plugin Marker for the matrix to image conversion.
	delete outputSeriesSpan;

	return outImage;
}



int main(int argc, char** argv) {

	Mat image, compressed;
	// Image file path.
	String imagePath = "C:/Users/anton/Desktop/rally.png";
	// Quadtree algorithm split treshold (a bigger value means a more compressed image).
	double threshold = 5;

	// Start of the Concurrency Visualizer Plugin Marker for the image reading operation.
	marker_series inputSeries(_T("Image Reading"));
	span *inputSeriessSpan = new span(inputSeries, 1, _T("Image Input"));
	inputSeries.write_flag(1, _T("flag"));

	image = imread(imagePath, CV_LOAD_IMAGE_COLOR);
	if (!image.data)
	{
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	namedWindow("Original image", WINDOW_AUTOSIZE);
	imshow("Original image", image);
	waitKey(1);

	// End of the Concurrency Visualizer Plugin Marker for the image reading operation.
	delete inputSeriessSpan;

	compressed = quadtreeCompress(image, threshold);

	namedWindow("Compressed image", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Compressed image", compressed);
	waitKey(0);

	return 0;

}




