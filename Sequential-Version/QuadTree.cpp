
#include "QuadTree.h"

QuadTree::QuadTree(int data[], int matWidth, int matHeight, double threshold) {
	root = new Node(data, matHeight, 0, 0, matWidth, matHeight, threshold);
}

int QuadTree::get(int i, int j) {
	return root->get(i, j);
}
